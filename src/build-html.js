const glob = require('glob');
const fs = require('fs');
const _ = require('underscore');
const handlebars = require('handlebars');

function sorter(path) {
  return path;
}

const files = _.sortBy(glob.sync('src/slides/*.html'), sorter);


const readFiles = files.map(function (file) {
  return fs.readFileSync(file).toString();
});

const context = {
  slides: new handlebars.SafeString(readFiles.join(''))
};

const template = handlebars.compile(fs.readFileSync('src/index.html').toString());

fs.writeFileSync('build/index.html', template(context));
