var Reveal = require('reveal.js');

window.Reveal = Reveal;

Reveal.initialize({
  controls: true,
  progress: true,
  history: true,
  center: true,

  width: 1500,
  height: 800,

  transition: 'slide',
  backgroundTransition: 'zoom',

  dependencies: [{
    src: 'plugin/highlight/highlight.js',
    async: true,
    callback: function () { hljs.initHighlightingOnLoad() }
  }]
});
